<?php

namespace GSix\ModuleGenerator\Generators;

interface GeneratorInterface
{
    public function generate();
}