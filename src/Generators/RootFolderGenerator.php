<?php

namespace GSix\ModuleGenerator\Generators;



class RootFolderGenerator implements GeneratorInterface
{
    /**
     * @var FileSystemGeneratorInterface
     */
    private $fileSystemGenerator;
    /**
     * @var
     */
    private $name;

    /**
     * @param FileSystemGeneratorInterface $fileSystemGenerator
     * @param $name
     */
    public function __construct(
        FileSystemGeneratorInterface $fileSystemGenerator,
        $name
    ) {
        $this->fileSystemGenerator = $fileSystemGenerator;
        $this->name = $name;
    }

    public function generate()
    {
        $folder = $this->fileSystemGenerator->getRootFolder(). "/" . $this->name;
        return $this->fileSystemGenerator->createFolder($folder);
    }
}