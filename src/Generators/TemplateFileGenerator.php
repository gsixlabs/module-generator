<?php

namespace GSix\ModuleGenerator\Generators;

class TemplateFileGenerator implements GeneratorInterface
{
    private $namespace;
    private $name;
    private $folder;
    /**
     * @var FileSystemGeneratorInterface
     */
    private $fileSystem;
    /**
     * @var
     */
    private $template;
    /**
     * @var
     */
    private $data;

    /**
     * @param FileSystemGeneratorInterface $fileSystem
     * @param $namespace
     * @param $name
     * @param $folder
     */
    function __construct(
        FileSystemGeneratorInterface $fileSystem,
        $template,
        $data = [],
        $namespace,
        $name,
        $folder
    ) {
        $this->fileSystem = $fileSystem;
        $this->template = $template;
        $this->data = $data;
        $this->namespace = $namespace;
        $this->name = $name;
        $this->folder = $folder;
    }

    /**
     * Generates the Eloquent model in the Module's root folder
     * @return string
     */
    public function generate()
    {
        $bluePrint = $this->fileSystem->loadBlueprint($this->template);
        $bluePrint = $this->fileSystem->fillBlueprint(
            $bluePrint,$this->data
        );

        if ($this->fileSystem->createFile($this->folder, $this->name, $bluePrint)) {
            return $this->folder.$this->name.".php";
        }

        return false;
    }
}