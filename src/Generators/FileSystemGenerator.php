<?php

namespace GSix\ModuleGenerator\Generators;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Application;

class FileSystemGenerator implements FileSystemGeneratorInterface
{
    /**
     * @var Filesystem
     */
    private $fileSystem;
    /**
     * @var Application
     */
    private $app;

    /**
     * @param Application $app
     * @param Filesystem $fileSystem
     */
    public function __construct(Application $app, Filesystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
        $this->app = $app;
    }

    /**
     * Creates a folder
     * @param string $name
     * @return bool|string
     */
    public function createFolder($path = '')
    {
        if ($path === "") {
            throw new \InvalidArgumentException("Missing argument PATH");
        }

        if ($this->fileSystem->makeDirectory($path, 0755, false, true)) {
            return $path . "/";
        }

        return false;
    }

    /**
     * Create a file
     * @param string $path
     * @param string $name
     * @param string $content
     * @return int
     */
    public function createFile($path = '', $name = '', $content = '')
    {
        if($path === "") {
            throw new \InvalidArgumentException("Missing argument PATH");
        }

        if($name === "") {
            throw new \InvalidArgumentException("Missing argument NAME");
        }

        if($content === "") {
            throw new \InvalidArgumentException("Missing argument CONTENT");
        }

        return $this->fileSystem->put($path . $name . ".php", $content);
    }


    /**
     * Loads the specified Blueprint
     * @param string $name
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function loadBlueprint($name = '')
    {
        return $this->fileSystem->get(
            __DIR__ . "/../Blueprints/" . $name . ".bp"
        );
    }

    /**
     * Replaces placeholder values in the blueprint
     * @param $blueprint
     * @param array $data
     * @return mixed
     */
    public function fillBlueprint($blueprint, $data = [])
    {
        return str_replace(array_keys($data), array_values($data), $blueprint);
    }

    function getRootFolder()
    {
        return $this->app->path();
    }

    function getAppNamespace()
    {
        return $this->app->getNamespace();
    }

    function deleteFolder($path = '')
    {
        return $this->fileSystem->deleteDirectory($path);
    }

    function folderExists($path = '')
    {
        return $this->fileSystem->isDirectory($path);
    }
}