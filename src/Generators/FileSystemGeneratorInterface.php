<?php

namespace GSix\ModuleGenerator\Generators;

interface FileSystemGeneratorInterface
{

    function getRootFolder();

    function getAppNamespace();

    function folderExists($path = '');

    /**
     * Creates a folder
     * @param string $name
     * @return bool|string
     */
    function createFolder($path = '');

    function deleteFolder($path = '');

    /**
     * Create a file
     * @param string $path
     * @param string $name
     * @param string $content
     * @return int
     */
    function createFile($path = '', $name = '', $content = '');


    /**
     * Loads the specified Blueprint
     * @param string $name
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    function loadBlueprint($name = '');

    /**
     * Replaces placeholder values in the blueprint
     * @param $blueprint
     * @param array $data
     * @return mixed
     */
    function fillBlueprint($blueprint, $data = []);

}