<?php

namespace GSix\ModuleGenerator\Generators;



class SubFolderGenerator implements GeneratorInterface
{
    /**
     * @var FileSystemGeneratorInterface
     */
    private $fileSystemGenerator;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $parent;

    /**
     * @param FileSystemGeneratorInterface $fileSystemGenerator
     * @param $name
     */
    public function __construct(
        FileSystemGeneratorInterface $fileSystemGenerator,
        $parent,
        $name
    ) {
        $this->fileSystemGenerator = $fileSystemGenerator;
        $this->parent = $parent;
        $this->name = $name;
    }

    public function generate()
    {
        $folder = $this->parent . $this->name;
        return $this->fileSystemGenerator->createFolder($folder);
    }
}