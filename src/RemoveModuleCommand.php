<?php

namespace GSix\ModuleGenerator;

use GSix\ModuleGenerator\Generators\FileSystemGenerator;
use GSix\ModuleGenerator\Generators\RootFolderGenerator;
use GSix\ModuleGenerator\Generators\SubFolderGenerator;
use GSix\ModuleGenerator\Generators\TemplateFileGenerator;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Application;
use Symfony\Component\Console\Input\InputArgument;

class RemoveModuleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:remove {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes a Module.';

    /**
     * @var FileSystemGenerator
     */
    private $fileSystem;

    /**
     * @param Application $app
     * @param Filesystem $file
     */
    public function __construct(Application $app, Filesystem $file)
    {
        parent::__construct();
        $this->fileSystem = new FileSystemGenerator($app, $file);
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
     public function handle()
     {
         $moduleName = $this->argument('name');

         if($this->askRemoveFolder($moduleName)) {
             $this->line("Removing <info>$moduleName</info>");
             $this->fileSystem->deleteFolder($this->fileSystem->getRootFolder() . "/" . $moduleName);
             $this->infoRemovedFolder($moduleName);
         }
     }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'Name of the Module'],
        ];

    }

    /**
     * @param $param
     */
    private function askRemoveFolder($param)
    {
        return $this->confirm("Are you sure you want to remove <info>$param</info>?");
    }

    private function infoRemovedFolder($param)
    {
        $this->line("Removed Folder  : <info>$param</info>");
    }

}