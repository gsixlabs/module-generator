<?php

namespace GSix\ModuleGenerator;

use Illuminate\Support\ServiceProvider;

class ModuleGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app['command.module.generate'] = $this->app->share(
            function ($app) {
                return new GenerateModuleCommand(
                    $app, $app['files']
                );
            }
        );

        $this->app['command.module.remove'] = $this->app->share(
            function ($app) {
                return new RemoveModuleCommand(
                    $app, $app['files']
                );
            }
        );

        $this->commands('command.module.generate');
        $this->commands('command.module.remove');
    }

    public function provides()
    {
        return [
            'command.module.generate',
            'command.module.remove'
        ];
    }

}