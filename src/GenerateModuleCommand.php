<?php

namespace GSix\ModuleGenerator;

use GSix\ModuleGenerator\Generators\FileSystemGenerator;
use GSix\ModuleGenerator\Generators\RootFolderGenerator;
use GSix\ModuleGenerator\Generators\SubFolderGenerator;
use GSix\ModuleGenerator\Generators\TemplateFileGenerator;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Application;
use Symfony\Component\Console\Input\InputArgument;

class GenerateModuleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:generate {name} {--clean}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates a new Module and it\'s structure.';

    /**
     * @var FileSystemGenerator
     */
    private $fileSystem;

    /**
     * @param Application $app
     * @param Filesystem $file
     */
    public function __construct(Application $app, Filesystem $file)
    {
        parent::__construct();
        $this->fileSystem = new FileSystemGenerator($app, $file);
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $moduleName = $this->argument('name');
        $moduleNamespace = $this->fileSystem->getAppNamespace();

        if($this->option('clean')) {
            $this->line("Removing <info>$moduleName</info>");
            $this->fileSystem->deleteFolder($this->fileSystem->getRootFolder() . "/" . $moduleName);
        }

        if($this->fileSystem->folderExists($this->fileSystem->getRootFolder() . "/" . $moduleName)) {
            $this->error($moduleName . " exists. Use the --clean option to remove it or remove it manually.");
        }

        // Create the root folder of the module
        $rootFolder = (
            new RootFolderGenerator($this->fileSystem, $moduleName)
        )->generate();

        if ($rootFolder) {
            $this->infoCreatedFolder($rootFolder);
        }

        // Create the Model
        $model = (
            new TemplateFileGenerator(
                $this->fileSystem,
                "Model",
                [
                    '#MODULE_NAMESPACE#' => $moduleNamespace,
                    '#MODULE_NAME#' => $moduleName
                ],
                $moduleNamespace, $moduleName, $rootFolder)
        )->generate();

        if ($model) {
            $this->infoCreatedFile($model);
        }



        // Create the Repositories folder
        $repositoriesFolder = (
            new SubFolderGenerator($this->fileSystem, $rootFolder, "Repositories")
        )->generate();

        if($repositoriesFolder) {
            $this->infoCreatedFolder($repositoriesFolder);
        }


        // Create the RepositoryInterface file
        $repositoryInterface = (
            new TemplateFileGenerator(
                $this->fileSystem,
                "RepositoryInterface",
                [
                    '#MODULE_NAMESPACE#' => $moduleNamespace,
                    '#MODULE_NAME#' => $moduleName
                ],
                $moduleNamespace, $moduleName."RepositoryInterface", $repositoriesFolder
            )
        )->generate();

        if ($repositoryInterface) {
            $this->infoCreatedFile($repositoryInterface);
        }


        // Create the Eloquent Repository
        $eloquentRepository = (
            new TemplateFileGenerator(
                $this->fileSystem,
                "EloquentRepository",
                [
                    '#MODULE_NAMESPACE#' => $moduleNamespace,
                    '#MODULE_NAME#' => $moduleName
                ],
                $moduleNamespace, "Eloquent".$moduleName."Repository", $repositoriesFolder
            )
        )->generate();

        if ($eloquentRepository) {
            $this->infoCreatedFile($eloquentRepository);
        }

        // Create the Controllers folder
        $controllersFolder = (
            new SubFolderGenerator($this->fileSystem, $rootFolder, "Controllers")
        )->generate();

        if ($controllersFolder) {
            $this->infoCreatedFolder($controllersFolder);
        }

        // Create Controllers/Admin folder
        $controllersAdminFolder = (
            new SubFolderGenerator($this->fileSystem, $controllersFolder, "Admin")
        )->generate();

        if ($controllersAdminFolder) {
            $this->infoCreatedFolder($controllersAdminFolder);
        }

        // Create Controllers/Web folder
        $controllersWebFolder = (
            new SubFolderGenerator($this->fileSystem, $controllersFolder, "Web")
        )->generate();

        if ($controllersWebFolder) {
            $this->infoCreatedFolder($controllersWebFolder);
        }

        // Create the Admin Controller file
        $adminController = (
        new TemplateFileGenerator(
            $this->fileSystem,
            "Controller",
            [
                '#MODULE_NAMESPACE#' => $moduleNamespace,
                "#SUB_NAMESPACE#" => "Admin",
                '#MODULE_NAME#' => $moduleName
            ],
            $moduleNamespace, $moduleName."Controller", $controllersAdminFolder
        )
        )->generate();

        if ($adminController) {
            $this->infoCreatedFile($adminController);
        }

        // Create the Web Controller file
        $webController = (
            new TemplateFileGenerator(
                $this->fileSystem,
                "Controller",
                [
                    '#MODULE_NAMESPACE#' => $moduleNamespace,
                    "#SUB_NAMESPACE#" => "Web",
                    '#MODULE_NAME#' => $moduleName
                ],
                $moduleNamespace, $moduleName."Controller", $controllersWebFolder
            )
        )->generate();

        if ($webController) {
            $this->infoCreatedFile($webController);
        }

        // Create the ServiceProviders folder
        $serviceProvidersFolder = (
            new SubFolderGenerator($this->fileSystem, $rootFolder, "ServiceProviders")
        )->generate();

        if ($serviceProvidersFolder) {
            $this->infoCreatedFolder($serviceProvidersFolder);
        }

        // Create the ServiceProvider file
        $serviceProviderFile = (
            new TemplateFileGenerator(
                $this->fileSystem,
                "RepositoryServiceProvider",
                [
                    '#MODULE_NAMESPACE#' => $moduleNamespace,
                    '#MODULE_NAME#' => $moduleName
                ],
                $moduleNamespace, $moduleName."RepositoryServiceProvider", $serviceProvidersFolder
            )
        )->generate();

        if ($serviceProviderFile) {
            $this->infoCreatedFile($serviceProviderFile);
        }

        // Create the RouteServiceProvider file
        $routeServiceProviderFile = (
            new TemplateFileGenerator(
                $this->fileSystem,
                "RouteServiceProvider",
                [
                    '#MODULE_NAMESPACE#' => $moduleNamespace,
                    '#MODULE_NAME#' => $moduleName,
                    '#PREFIX#' => strtolower($moduleName)
                ],
                $moduleNamespace, $moduleName."RoutesServiceProvider", $serviceProvidersFolder
            )
        )->generate();

        if ($routeServiceProviderFile) {
            $this->infoCreatedFile($routeServiceProviderFile);
        }

        // Create the Routes folder
        $routesFolder = (
            new SubFolderGenerator($this->fileSystem, $rootFolder, "Routes")
        )->generate();

        if ($routesFolder) {
            $this->infoCreatedFolder($routesFolder);
        }

        // Create the admin routes file
        $adminRoutesFile = (
            new TemplateFileGenerator(
                $this->fileSystem,
                "routes",
                [
                    '#MODULE_NAMESPACE#' => $moduleNamespace,
                    '#SUB_NAMESPACE#' => "Admin",
                    "#SUB_NAMESPACE_LOWERCASE#" => "admin",
                    '#MODULE_NAME#' => $moduleName,
                    "#MODULE_NAME_LOWERCASE#" => strtolower($moduleName),
                    '#PREFIX#' => "admin"
                ],
                $moduleNamespace, "admin", $routesFolder
            )
        )->generate();

        if ($adminRoutesFile) {
            $this->infoCreatedFile($adminRoutesFile);
        }

        // create the web routes file
        $webRoutesFile = (
            new TemplateFileGenerator(
                $this->fileSystem,
                "routes",
                [
                    '#MODULE_NAMESPACE#' => $moduleNamespace,
                    '#SUB_NAMESPACE#' => "Web",
                    "#SUB_NAMESPACE_LOWERCASE#" => "web",
                    '#MODULE_NAME#' => $moduleName,
                    "#MODULE_NAME_LOWERCASE#" => strtolower($moduleName),
                    '#PREFIX#' => strtolower($moduleName)
                ],
                $moduleNamespace, "web", $routesFolder
            )
        )->generate();

        if ($webRoutesFile) {
            $this->infoCreatedFile($webRoutesFile);
        }

        // Create the Middlewares folder
        $middlewaresFolder = (
            new SubFolderGenerator($this->fileSystem, $rootFolder, "Middlewares")
        )->generate();

        if ($middlewaresFolder) {
            $this->infoCreatedFolder($middlewaresFolder);
        }

        // Create Service folder
        $servicesFolder = (
            new SubFolderGenerator($this->fileSystem, $rootFolder, "Services")
        )->generate();

        if ($servicesFolder) {
            $this->infoCreatedFolder($servicesFolder);
        }

        // Create the Policies folder
        $policiesFolder = (
            new SubFolderGenerator($this->fileSystem, $rootFolder, "Policies")
        )->generate();

        if ($policiesFolder) {
            $this->infoCreatedFolder($policiesFolder);
        }

        // Create ViewComposers folder
        $viewComposersFolder = (
            new SubFolderGenerator($this->fileSystem, $rootFolder, "ViewComposers")
        )->generate();

        if ($viewComposersFolder) {
            $this->infoCreatedFolder($viewComposersFolder);
        }

        $this->line("Done!");

    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'Name of the Module'],
        ];

    }

    /**
     * @param $param
     */
    private function infoCreatedFolder($param)
    {
        $this->line("Created Folder  : <info>$param</info>");
    }

    private function infoCreatedFile($param)
    {
        $this->line("Created File    : <info>$param</info>");
    }
}