# gsixlabs/module-generator

[![Latest Version on Packagist][ico-version]](https://packagist.org/packages/gsixlabs/module-generator)
[![Software License][ico-license]](LICENSE.md)
[![Latest Stable Version](https://poser.pugx.org/gsixlabs/module-generator/v/stable)](https://packagist.org/packages/gsixlabs/module-generator)
[![Latest Unstable Version](https://poser.pugx.org/gsixlabs/module-generator/v/unstable)](https://packagist.org/packages/gsixlabs/module-generator)
[![Daily Downloads](https://poser.pugx.org/gsixlabs/module-generator/d/daily)](https://packagist.org/packages/gsixlabs/module-generator)
[![Monthly Downloads](https://poser.pugx.org/gsixlabs/module-generator/d/monthly)](https://packagist.org/packages/gsixlabs/module-generator)
[![Total Downloads](https://poser.pugx.org/gsixlabs/module-generator/downloads)](https://packagist.org/packages/gsixlabs/module-generator)


This package was created to help us bootstrap our development of Laravel "modules" according to our internal structure.
Therefore it is opinionated.

## Install

Via Composer

``` bash
$ composer require --dev gsixlabs/module-generator
```

Add the service provider to `config/app`

```
GSix\ModuleGenerator\ModuleGeneratorServiceProvider::class,
```

## Troubleshot

If the composer installation fails, try removing hirak/prestissimo

```
composer global remove hirak/prestissimo
```

## Usage

```
php artisan module:generate {name} {--clean}
```


## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Security

If you discover any security related issues, please email :author_email instead of using the issue tracker.

## Credits

- [Vlatko Gjurgjinoski][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/:vendor/:package_name.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/:vendor/:package_name/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/:vendor/:package_name.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/:vendor/:package_name.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/:vendor/:package_name.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/:vendor/:package_name
[link-travis]: https://travis-ci.org/:vendor/:package_name
[link-scrutinizer]: https://scrutinizer-ci.com/g/:vendor/:package_name/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/:vendor/:package_name
[link-downloads]: https://packagist.org/packages/:vendor/:package_name
[link-author]: https://github.com/:author_username
[link-contributors]: ../../contributors
